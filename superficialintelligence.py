from reportlab.platypus import *
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.units import inch, cm
from reportlab.pdfbase.ttfonts import TTFont, pdfmetrics

import sys, argparse, re
# import html5lib
 



ap = argparse.ArgumentParser("")
ap.add_argument("--font", default="OSP-DIN.ttf")
ap.add_argument("--margin", default=0.6, type=float)
ap.add_argument("--svg", default=None)
ap.add_argument("--image", default="images/gg0610b_000507.jpg")
args = ap.parse_args()
if args.image and not args.svg:
    args.svg = args.image.replace(".jpg", ".word.svg")

margin = args.margin*cm
font =  TTFont('MyFontName', args.font)
pdfmetrics.registerFont(font)

styles = getSampleStyleSheet()
ps = styles['Normal']
ps.fontName = "MyFontName"
ps.fontSize = 10
ps.leading = 10
ps.backColor = "white"
# ps.borderPadding = 4

leftPadding = margin
rightPadding = margin
bottomPadding = margin
topPadding = margin
 
pagew, pageh = landscape(A4)
cwidth = pagew - margin*2
cheight = pageh - margin*2
framew = (cwidth / 2)
frameh = cheight
showBoundary = 0
leftColumn = Frame(margin, margin, framew/2, frameh, leftPadding=margin, bottomPadding=margin, rightPadding=margin, topPadding=margin, id="leftColumn", showBoundary=showBoundary)
rightColumn = Frame((pagew/2), margin, framew, frameh, leftPadding=0, bottomPadding=0, rightPadding=margin, topPadding=margin, id="rightColumn", showBoundary=showBoundary)
elements = []



# doc = SimpleDocTemplate("output.pdf", leftMargin=margin, topMargin=margin, pagesize=(pagew, pageh))
# doc.build(elements, onFirstPage=drawPage)

from PIL import Image
from xml.etree import ElementTree as ET 

def rects (t):
    for g in t.findall(".//{http://www.w3.org/2000/svg}g[@transform]"):
        x = g.attrib.get("transform")
        m = re.search(r"translate\((\d+),(\d+)\)", x)
        if m:
            x, y = [int(x) for x in (m.groups())]
            rect = g.find(".//{http://www.w3.org/2000/svg}rect")
            text = g.find(".//{http://www.w3.org/2000/svg}text")
            t = text.text
            # set the text style font size to match rect height
            w, h = int(rect.attrib.get("width")), int(rect.attrib.get("height"))
            yield(x, y, w, h, t)

def overlapping (ax, ay, aw, ah, bx, by, bw, bh):
    ax1, ay1, ax2, ay2 = ax, ay, ax+aw, ay+ah
    bx1, by1, bx2, by2 = bx, by, bx+bw, by+bh
    return (ax1 < bx2 and ax2 > bx1 and ay1 < by2 and ay2 > by1)

# draw word.svg
def draw_to_canvas(canvas, svgpath, imagepath, imagerect=None):
    svg = ET.parse(svgpath)
    im = Image.open(imagepath)
    if imagerect == None:
        # fullimage
        # imagerect = (0, 0, im.size[0], im.size[1])
        # midimage
        iw, ih = im.size
        imagerect = (iw/4, ih/4+800, iw/2, ih/2)

    # Fit the imagerect in the page
    ix, iy, iw, ih = imagerect
    scale = max(cheight / ih, cwidth / iw)

    def i2c (x, y, w, h):
        nx = (x - ix) * scale
        ny = cheight - ((y - iy) * scale)
        nw = w * scale
        nh = h * scale
        return nx, ny, nw, nh

    for x, y, w, h, t in rects(svg):
        # print (x, y, w, h, t)
        # print (t)
        px, py, pw, ph = i2c(x, y, w, h)
        if overlapping(px, py, pw, ph, 0, 0, pagew, pageh):
            canvas.setFillColorRGB(0,0,0)
            canvas.rect(px,py,pw, ph, fill=1)
            canvas.setFillColorRGB(1,1,1)
            canvas.drawString(px, py, t)
        # else:
        #     print ("skiping rect", x, y, w, h, file=sys.stderr)

tt = ET.parse("borges.xml")
for i, p in enumerate(tt.findall(".//para")):
    psrc = ET.tostring(p, method="xml", encoding="unicode").strip()
    print ("adding paragraph |{0}|".format(psrc), file=sys.stderr)
    para = Paragraph(psrc, styles["Normal"])
    elements.append(para)
    # if i == 1:
    #     break

def drawPage (c, doc):
    c.saveState()
    margin = 0.6*cm
    d = margin
    if showBoundary:
        c.line(d, 0, d, pageh)
        c.line(pagew-d, 0, pagew-d, pageh)

        c.line(0, d, pagew, d)
        c.line(0, pageh-d, pagew, pageh-d)

        # X
        c.line(margin, margin, pagew-margin, pageh-margin)
        c.line(margin, pageh-margin, pagew-margin, margin)
    draw_to_canvas(c, args.svg, args.image)
    c.restoreState()

# FrameBreak
elements.append(CondPageBreak(cheight))
# p('hello')
page = PageTemplate(id="base", frames=[leftColumn, rightColumn], pagesize=landscape(A4))
page.beforeDrawPage = drawPage
doc = BaseDocTemplate("superficialintelligence.sicv.pdf", pageTemplates=[page])




doc.build(elements)
